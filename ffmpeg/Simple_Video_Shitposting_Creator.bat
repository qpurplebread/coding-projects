@echo off
:first
set /p image="Drag Image File: "
set /p audio="Drag Audio File: "
set /p output="Output Name and Extension: "
ffmpeg -loop 1 -i %image% -i %audio% -c:v libx264 -tune stillimage -c:a aac -b:a 192k -pix_fmt yuv420p -shortest "%USERPROFILE%\Desktop\YouTube-DL\Conversions\%output%"
echo.
goto first
