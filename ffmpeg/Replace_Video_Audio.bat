@echo off
:first
set /p video="Drag Video File: "
set /p audio="Drag Audio File: "
set /p output="Output Name and Extension: "
ffmpeg -i %video% -i %audio% -c:v copy -map 0:v:0 -map 1:a:0 -shortest "%USERPROFILE%\Desktop\YouTube-DL\Conversions\%output%"
echo.
goto first
