@echo off
color 0b
:first
set /p input="Drag and Drop Video File: "
set /p timestart="Start time (HH:MM:SS): "
set /p timeafter="Cut video after (HH:MM:SS) has passed: "
set /p output="Output Name and Extension: "
ffmpeg -ss %timestart% -i %input% -to %timeafter% -c copy "%USERPROFILE%\Desktop\YouTube-DL\%output%"
echo.
goto first