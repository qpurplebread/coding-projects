@echo off
:first
set /p input1="Drag and Drop 1st Video File: "
set /p input2="Drag and Drop 2nd Video File: "
set /p output="Output Name and Extension: "
ffmpeg -i %input1% -c copy -bsf:v h264_mp4toannexb -f mpegts 01.ts
ffmpeg -i %input2% -c copy -bsf:v h264_mp4toannexb -f mpegts 02.ts
ffmpeg -i "concat:01.ts|02.ts" -c copy -bsf:a aac_adtstoasc "%USERPROFILE%\Desktop\YouTube-DL\Conversions\%output%"
del 01.ts
del 02.ts
echo.
goto first